$j = jQuery.noConflict();

function ajaxRequestGet(sitePart, name = 'default.html', url = null,  data = null) {
    var result='';

    if(url == null) {
        url = '../partials/' + sitePart + '/' + name;
    }
    
    $j.ajax({
        'async': false,
        'type': 'GET',
        'dataType': 'html',
        'url': url,
        'data': data,
        'success': function (data) {
            result = data;      
        }
    });
    return result;
}

